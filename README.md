## Steps & Command list:  
\# install python & pip on the system  
\# install Django  
**$ pip install django**  
\# create a project folder with all necessary files  
**$ django-admin startproject django_hello_world**  
\# create hello app inside of the project created  
**$ python3 manage.py startapp hello_app**  
\# Here code the app, creating a view method and adding a routing in urls file  
\# then run server  
**$ python3 manage.py runserver**  
