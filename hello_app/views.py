from django.shortcuts import render
# to response is necessary import form django.http module HttpResponse
from django.http import HttpResponse

# Create your views here.

# this method will return my hello code for the view
def index_hello(request):
    return HttpResponse('<h1>Hello world of Django my friend!! :)</h1>')
